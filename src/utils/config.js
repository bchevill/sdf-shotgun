const prod = {
    urls: {
        URL: "https://shotshut.assos.utc.fr/", // URL de l'API
        API_URL: "https://shotshut.assos.utc.fr/api/event/3", // URL de l'API
    },
};

const dev = {
    urls: {
        URL: "https://pktuchercheici.picasso-utc.fr", // URL de l'API
        API_URL: "https://pktuchercheici.picasso-utc.fr/api/", // URL de l'API
    },
};

const local = {
    urls: {
        URL: "http://localhost:8000", // URL de l'API
        API_URL: "http://localhost:8000/api/", // URL de l'API
    },
};

const defaultConfig = {
    context: "tnAEwd114GDHMklN7r6WDn55XhY6o3f3"
}
export const PUBLIC_URL = process.env.PUBLIC_URL || "";

export function asset_url(path) {
    return PUBLIC_URL + path;
}

export const config = (() => {
    const urls = prod.urls;
    return {
        ...defaultConfig,
        urls,
    };
})();
