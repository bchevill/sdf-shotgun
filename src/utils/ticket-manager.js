import CryptoJS from "crypto-js";
import {config} from "./config";

const myTicketFounder = config.context;

export function prepareTicketKey(ticketFounder) {
    //hash take string and return {[5-15][0-5][15-20][20-][10-15][5-10]}
    return ticketFounder.slice(5, 15) + ticketFounder.slice(0, 5) + ticketFounder.substring(15, 20) + ticketFounder.slice(20) + ticketFounder.substring(10, 15) + ticketFounder.substring(5, 10);
}

export function getStorageTickets() {
    //"tickets" is a map {[key:email]:[...{tickets}]} crypted with AES and event context key
    try {
        const data = localStorage.getItem("tickets");
        if (data) {
            const bytes = CryptoJS.AES.decrypt(data, prepareTicketKey(myTicketFounder));
            if (!bytes) return null;
            return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        }
    } catch (e) {
        //if error, remove tickets
        localStorage.removeItem("tickets");
    }
    return {};
}

export function getStorageTicketAsArray() {
    const tickets = getStorageTickets();
    return Object.keys(tickets).map((k) => tickets[k]).flat();
}

export function setStorageTickets(tickets) {
    //"tickets" is a map {[key:email]:[...{tickets}]} crypted with AES and event context key
    const data = CryptoJS.AES.encrypt(
        JSON.stringify(tickets),
        prepareTicketKey(myTicketFounder)
    ).toString();
    localStorage.setItem("tickets", data);
}

export function mergeStorageTickets(email, tickets) {
    const storageTickets = getStorageTickets();
    if (storageTickets[email]) {
        const ids = storageTickets[email].map((t) => t.id);
        tickets = tickets.filter((t) => !ids.includes(t.id));
        storageTickets[email] = storageTickets[email].concat(tickets);
    } else {
        storageTickets[email] = tickets;
    }
    setStorageTickets(storageTickets);
}