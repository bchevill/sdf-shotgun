import {configureStore, createSlice} from '@reduxjs/toolkit'

const eventSlice = createSlice({
    name: 'event-store',
    initialState: {
        value: {}
    },
    reducers: {
        saveEvent: (state, action) => {
            state.value = action.payload
        },
        clearEvent: (state) => {
            state.value = {}
        },
        getEvent: (state) => {
            return state.value
        }
    }
})

export const {saveEvent, clearEvent, getEvent} = eventSlice.actions

export const store = configureStore({
    reducer: {
        event: eventSlice.reducer
    }
})