import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import {createBrowserRouter, RouterProvider} from "react-router-dom";
import Root from "./Root";
import NoPage from "./pages/NoPage";
import Tickets from "./pages/Tickets";
import ShowShotguns from "./pages/ShowShotguns";
import ShowShotgun from "./pages/ShowShotgun";
import Welcome from "./pages/Welcome";
import {Provider} from "react-redux";
import {store as eventStore} from "./store/store";

const router = createBrowserRouter([
    {
        path: "/",
        element: <Root/>,
        errorElement: <NoPage/>,
        children: [
            {
                path: "/",
                element: <Welcome/>,
            },
            {
                path: "tickets",
                element: <Tickets/>,
            },
            {
                path: "shotguns",
                element: <ShowShotguns/>,
            },
            {
                path: "shotguns/:id",
                element: <ShowShotgun/>,
            },
            {
                path: "*",
                element: <NoPage/>,
            },
        ],
    },
]);

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
    <Provider store={eventStore}>
        <React.StrictMode>
            <RouterProvider router={router}/>
        </React.StrictMode>
    </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
