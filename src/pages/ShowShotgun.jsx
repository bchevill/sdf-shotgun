import {useEffect, useState} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {useSelector} from "react-redux";
import {getStorageTicketAsArray, getStorageTickets, setStorageTickets} from "../utils/ticket-manager";
import {DirectionsBus, LocalActivity} from "@mui/icons-material";
import {ajaxDelete, ajaxGet, ajaxPost} from "../utils/ajax";
import {Snackbar} from "@mui/material";
import CryptoJS from "crypto-js";

const ShowShotgun = () => {
    const navigate = useNavigate();
    const shotgunId = useParams().id;
    const [shotgun, setShotgun] = useState(undefined);
    const [tickets, setTickets] = useState(undefined);
    const [selectedTicket, setSelectedTicket] = useState(undefined);
    const [selectedShuttle, setSelectedShuttle] = useState(undefined);
    const [shuttles, setShuttles] = useState([]);
    const [snackbar, setSnackbar] = useState(undefined);
    const [shotgunOpen, setShotgunOpen] = useState(false);
    const event = useSelector((state) => state.event.value);

    const loadShuttles = () => {
        return ajaxGet(`/shotguns/${shotgunId}/shuttles`)
    }
    //create interval to reload shotgunOpen state
    const reloadShotgunOpen = () => {
        setInterval(() => {
            setShotgunOpen(new Date() > new Date(shotgun.start_date));
        }, 1000 * 5)
    }

    useEffect(() => {
        if (!event || !event.shotguns) {
            return;
        }
        const shotgun = event.shotguns.find((shotgun) => shotgun.id.toString() === shotgunId);
        if (!shotgun) {
            navigate("/shotguns");
            return;
        }
        const tickets = getStorageTicketAsArray()
        //sort non-booked tickets first
        tickets.sort((a, b) => {
            if (ticketHaveBooking(a) && !ticketHaveBooking(b)) {
                return 1;
            }
            if (!ticketHaveBooking(a) && ticketHaveBooking(b)) {
                return -1;
            }
            return 0;
        })
        setTickets(tickets);
        if (tickets && tickets.length > 0 && !selectedTicket) {
            setSelectedTicket(tickets[0]);
        }
        setShotgun(shotgun);
        //load shuttles
        const shuttles = [];
        loadShuttles().then((response) => {
            response.data.forEach((shuttle) => {
                shuttles.push(shuttle);
            })
            shuttles.sort((a, b) => {
                //sort by date
                return new Date(a.date) - new Date(b.date);
            })
            //sort also by bookings_count (bookings_count >= limit_place) last
            shuttles.sort((a, b) => {
                if (a.bookings_count >= a.limit_place && b.bookings_count < b.limit_place) {
                    return 1;
                }
                if (a.bookings_count < a.limit_place && b.bookings_count >= b.limit_place) {
                    return -1;
                }
                return 0;
            })
            setShuttles(shuttles);
        })
    }, [event])

    useEffect(() => {
        if (!shotgun) {
            return;
        }
        reloadShotgunOpen();
        setShotgunOpen(new Date() > new Date(shotgun.start_date));
    }, [shotgun])
    const getHour = (date) => {
        const dateObject = new Date(date);
        return dateObject.getUTCHours().toString().padStart(2, '0') + "h" + dateObject.getMinutes().toString().padStart(2, '0');
    }

    const isTicketSelected = (ticket) => {
        return selectedTicket && selectedTicket.id === ticket.id;
    }

    const isShuttleSelected = (shuttle) => {
        return selectedShuttle && selectedShuttle.id === shuttle.id;
    }

    function prepareBookingPayload(selectedTicket) {
        //at is timestamp of UTC date
        const at = new Date().getTime();
        const out = JSON.stringify({
            "ticket_id": selectedTicket.id,
            "email": selectedTicket.email,
            "at": Math.floor(at / 1000)
        });
        //payload is <base64 encoded json stringified ticket_id, email, at>.<MD5 hash of payload with event.name as key>
        const key = btoa(event.name + '.' + Object.assign({}, event).id);
        return btoa(out) + "." + CryptoJS.HmacMD5(btoa(out), key).toString();
    }

    async function reserveShuttle() {
        if (!selectedShuttle || !selectedTicket || ticketHaveBooking(selectedTicket)) {
            if (ticketHaveBooking(selectedTicket)) {
                setSnackbar("Vous avez déjà réservé une navette pour ce billet");
                setSelectedShuttle(undefined)
            }
            return;
        }
        try {
            const res = await ajaxPost(`/shotguns/${shotgunId}/shuttles/${selectedShuttle.id}/book`, {
                "payload": prepareBookingPayload(selectedTicket)
            })
            // if state is 201 save new ticket in local storage
            if (res.status === 201) {
                const ticket = res.data;
                //get shuttle and merge with shuttles
                const shuttle = shuttles.indexOf((shuttle) => ticket.shuttle.id === selectedShuttle.id);
                if (shuttle !== -1) {
                    const newShuttles = shuttles;
                    newShuttles[shuttle] = ticket.shuttle;
                    setShuttles(newShuttles);
                }
                delete ticket.shuttle;
                const tickets = getStorageTickets();
                //find ticket in tickets (tickets is a map<email, tickets>)
                const ticketIndex = tickets[ticket.email].findIndex((t) => t.id === ticket.id);
                if (ticketIndex !== -1) {
                    tickets[ticket.email][ticketIndex] = ticket;
                    setStorageTickets(tickets);
                    setTickets(getStorageTicketAsArray());
                }
                setSelectedShuttle(undefined)
                setSelectedTicket(ticket);
            }
        } catch (e) {
            const message = e.response.data.message;
            if (message && message === "shuttle is full") {
                setSnackbar("La navette est pleine");
                //set selected shuttle to undefined and selected shuttle to full
                const shuttleIndex = shuttles.indexOf((shuttle) => shuttle.id === selectedShuttle.id);
                if (shuttleIndex !== -1) {
                    const newShuttles = shuttles;
                    newShuttles[shuttleIndex].bookings_count = newShuttles[shuttleIndex].limit_place;
                    setShuttles(newShuttles);
                }
                setSelectedShuttle(undefined);
                return;
            }
            if (message && message === "ticket already reserved") {
                setSnackbar("Vous avez déjà réservé une navette pour ce billet");
                return;
            }
            setSnackbar(message || "Une erreur est survenue lors de la réservation")
        }
    }

    const removeBooking = async () => {
        if (!selectedTicket) {
            return;
        }
        try {
            await ajaxDelete(`/shotguns/${shotgunId}/shuttles/${getContextShuttle(selectedTicket).id}/book`, {
                "payload": prepareBookingPayload(selectedTicket)
            }).then((res) => {
                if (res.status === 204) {
                    const shuttle = shuttles.find((shuttle) => shuttle.id === getContextShuttle(selectedTicket).id);
                    if (shuttle) {
                        const shuttleIndex = shuttles.indexOf(shuttle);
                        shuttle.bookings_count--;
                        const newShuttles = shuttles;
                        newShuttles[shuttleIndex] = shuttle;
                        setShuttles(newShuttles);
                    }
                    //locally search for ticket booking and remove it
                    const tickets = getStorageTickets();
                    const ticketIndex = tickets[selectedTicket.email].findIndex((t) => t.id === selectedTicket.id);
                    if (ticketIndex !== -1) {
                        tickets[selectedTicket.email][ticketIndex].bookings = tickets[selectedTicket.email][ticketIndex].bookings.filter((booking) => booking.shotgun_id !== shotgun.id);
                        const ticket = tickets[selectedTicket.email][ticketIndex];
                        setSelectedTicket(ticket);
                        setStorageTickets(tickets);
                        setTickets(getStorageTicketAsArray());
                    }
                }
            })
        } catch (e) {
            setSnackbar("Une erreur est survenue lors de l'annulation de la réservation")
        }
    }

    const ticketHaveBooking = (ticket) => {
        if (!ticket || !ticket.bookings || !shotgun) {
            return false;
        }
        return ticket.bookings.find((booking) => booking.shotgun_id == shotgun.id);
    }
    const getContextShuttle = (ticket) => {
        if (!ticket || !ticket.bookings) {
            return false;
        }
        const booking = ticket.bookings.find((booking) => booking.shotgun_id === shotgun.id);
        if (!booking) {
            return false;
        }
        const shuttle = shuttles.find((shuttle) => shuttle.id === booking.id);
        if (!shuttle) {
            return false;
        }
        return shuttle;
    }

    return (
        <div className={"max-w-2xl p-2 h-full m-auto"}>
            {shotgun &&
                (<div className={"relative flex flex-col"}>
                    <button
                        className={"bg-blue-600 rounded hover:bg-indigo-500 text-white text-sm p-1 mb-3 w-20 m-auto"}
                        onClick={() => navigate("/")}>Retour
                    </button>
                    <p className={"text-xl text-center"}>{shotgun.name}</p>
                    <p className={"text-sm text-center text-gray-700"}>{shotgun.description}</p>
                    <p className={"text-sm text-center text-gray-700 mb-0 pb-O"}>Tous les départs ont lieu depuis le parvis de BF.</p>
                    <p className={"text-sm text-center text-gray-700"}> Rendez-vous sur place 10 minutes avant le départ.</p>
                    <p>Mes billets</p>
                    <div className={"h-34 overflow-x-auto p-2 flex gap-2 w-full"}>
                        {tickets.length === 0 && (<p className={"text-center text-gray-700 w-full p-2 border rounded"}>Vous n'avez pas de billets</p>)}
                        {
                            tickets.map((ticket) => {
                                return (
                                    <div key={ticket.id}
                                         onClick={() => setSelectedTicket(ticket)}
                                         className={"p-2 w-28 cursor-pointer aspect-square rounded flex flex-col items-center justify-between " + (isTicketSelected(ticket) ? 'border-indigo-500 border-2' : 'border') + (ticketHaveBooking(ticket) ? ' opacity-60' : '')}>
                                        <LocalActivity className={"h-8"}/>
                                        <p className={"text-sm text-center"} style={{fontSize: "10px"}}>#{ticket.code.toUpperCase()}</p>
                                        <p className={"text-xs text-gray-700 text-center"}>{ticket.product_name}</p>
                                    </div>
                                )
                            })
                        }
                    </div>
                    {
                        ticketHaveBooking(selectedTicket) && (
                            <div className={"grid border border-red-400 bg-red-100 rounded my-2 p-2 gap-2"}>
                                <p className={"text-center"}>Vous avez déjà réservé une navette pour ce billet</p>
                                <p className={"text-center text-xl"}>{getContextShuttle(selectedTicket).name} à {getHour(getContextShuttle(selectedTicket).date)}</p>
                                <button
                                    className={"w-full p-2 rounded bg-red-500 text-white transition-colors hover:bg-red-600"}
                                    onClick={removeBooking}>Annuler la réservation
                                </button>
                            </div>
                        )
                    }
                    <p>Les navettes disponibles</p>
                    {selectedTicket && (<p className={"text-xs text-gray-700 mb-2"}>Pour le billet
                        #{selectedTicket.code.toUpperCase()}</p>)}
                    {!shotgunOpen && (<div className={"border border-red-700 bg-red-100 rounded my-2 p-2 gap-2 text-center"}>
                        <p>Le shotgun n'est pas encore ouvert.</p>
                        <p className={"text-xs text-gray-700"}>Il ouvrira automatiquement, ne rafraichis pas la page.</p>
                        </div>)}
                    <div className={"grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-6 gap-2 mb-2"}>
                        {shuttles.map((shuttle) => {
                            if (shuttle.bookings_count >= shuttle.limit_place) {
                                return;
                            }
                            return (
                                <div key={shuttle.id}
                                     onClick={() => isShuttleSelected(shuttle) ? setSelectedShuttle(undefined) : setSelectedShuttle(shuttle)}
                                     className={"p-2 aspect-square cursor-pointer rounded flex flex-col items-center justify-around border " + (isShuttleSelected(shuttle) ? 'border-indigo-500 border-2' : 'border-gray-300')}>
                                    <p className={"text-sm text-gray-700"}>{getHour(shuttle.date)}</p>
                                    <DirectionsBus className={"h-8"}/>
                                    <p className={"text-sm text-gray-700 mt-3 mb-0"}>{shuttle.bookings_count}/{shuttle.limit_place}</p>
                                </div>
                            )
                        })}
                    </div>
                    <p>Les navettes pleines</p>
                    <div className={"grid grid-cols-2 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-6 gap-2 mb-14"}>
                        {shuttles.map((shuttle) => {
                            if (shuttle.bookings_count < shuttle.limit_place) {
                                return;
                            }
                            return (
                                <div key={shuttle.id}
                                     className={"p-2 aspect-square rounded flex flex-col items-center justify-around border border-gray-300 opacity-60"}>
                                    <DirectionsBus className={"h-8"}/>
                                    <p className={"text-sm text-gray-700"}>{getHour(shuttle.date)}</p>
                                </div>
                            )
                        })}
                    </div>
                    <div className={"fixed bottom-0 left-0 w-full p-2"}>
                        <button
                            onClick={() => reserveShuttle()}
                            className={"w-full md:lg:w-64 p-2 rounded bg-indigo-500 text-white transition-colors hover:bg-indigo-600 disabled:opacity-50 disabled:cursor-not-allowed"}
                            disabled={(!selectedShuttle && !selectedTicket) || !shotgunOpen}>
                            {!shotgunOpen && "Le shotgun n'est pas encore ouvert"}
                            {shotgunOpen && !selectedTicket && "Sélectionnez un billet"}
                            {shotgunOpen && selectedTicket && !selectedShuttle && "Sélectionnez une navette"}
                            {shotgunOpen && selectedTicket && selectedShuttle && "Réserver"}
                        </button>
                    </div>
                </div>)
            }
            <Snackbar
                open={snackbar !== undefined}
                autoHideDuration={2000}
                onClose={() => setSnackbar(undefined)}
                message={snackbar}
            />
        </div>
    )
}

export default ShowShotgun;