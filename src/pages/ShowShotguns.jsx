import {useNavigate} from "react-router-dom";
import {useSelector} from "react-redux";

const ShowShotguns = () => {
    const navigate = useNavigate();
    const event = useSelector((state) => state.event.value);
    return (
        <div className={"p-2"}>
            <p className={"text-xl"}>Liste des shotguns</p>
            {event !== undefined && event.shotguns && event.shotguns.map((shotgun) => {
                return (
                    <div key={shotgun.id} className={"border p-2 my-2 cursor-pointer"} onClick={()=>navigate(`/shotguns/${shotgun.id}`)}>
                        <p className={"text-lg"}>{shotgun.name}</p>
                        <p className={"text-sm"}>{shotgun.description}</p>
                    </div>
                )
            })}
            {!event && <p>Chargement des shotguns...</p>}
            {event.shotguns && event.shotguns.length === 0 && <p>Aucun shotgun pour le moment</p>}
        </div>
    )
}

export default ShowShotguns;