import { useEffect, useState } from "react";
import { ajaxGet } from "../utils/ajax";
import { useNavigate } from "react-router-dom";
import { Box, Button } from "@mui/material";
import { ConfirmationNumberRounded } from "@mui/icons-material";

const Shotgun = () => {
  const [refresh, setRefresh] = useState(true);
  const navigate = useNavigate();
  const [event, setEvent] = useState({
    id: 1,
    name: "Test de shotgun",
    description: "Test",
  });

  const [selectedTicket, setSelectedTicket] = useState(undefined);

  const [tickets, setTickets] = useState([]);

  const [shotguns, setShotguns] = useState([
    {
      id: 1,
      name: "Nasa départ",
      description: "C'est parti nasal",
      start_date: "2024-01-06T15:50:14.000000Z",
      end_date: "2024-01-10T15:50:18.000000Z",
      event_id: 1,
      created_at: "2024-01-06T14:50:22.000000Z",
      updated_at: "2024-01-06T15:07:43.000000Z",
    },
  ]);

  const [selectedShuttle, setSelectedShuttle] = useState(undefined);

  const [shuttles, setShuttles] = useState({
    1: [
      {
        id: 1,
        name: "Test",
        date: "2024-01-12T15:50:59.000000Z",
        max_places: 60,
        limit_place: 45,
        bookings_count: 0,
        start_location: {
          id: 2,
          name: "Benjamin franklin",
        },
        end_location: {
          id: 1,
          name: "Site mistère !",
        },
      },
      {
        id: 2,
        name: "Test2",
        date: "2024-01-12T15:50:59.000000Z",
        max_places: 60,
        limit_place: 40,
        bookings_count: 0,
        start_location: {
          id: 2,
          name: "Benjamin franklin",
        },
        end_location: {
          id: 1,
          name: "Site mistère !",
        },
      },
      {
        id: 2,
        name: "Test2",
        date: "2024-01-12T15:50:59.000000Z",
        max_places: 60,
        limit_place: 40,
        bookings_count: 0,
        start_location: {
          id: 2,
          name: "Benjamin franklin",
        },
        end_location: {
          id: 1,
          name: "Site mistère !",
        },
      },
      {
        id: 2,
        name: "Test2",
        date: "2024-01-12T15:50:59.000000Z",
        max_places: 60,
        limit_place: 40,
        bookings_count: 0,
        start_location: {
          id: 2,
          name: "Benjamin franklin",
        },
        end_location: {
          id: 1,
          name: "Site mistère !",
        },
      },
      {
        id: 2,
        name: "Test2",
        date: "2024-01-12T15:50:59.000000Z",
        max_places: 60,
        limit_place: 40,
        bookings_count: 0,
        start_location: {
          id: 2,
          name: "Benjamin franklin",
        },
        end_location: {
          id: 1,
          name: "Site mistère !",
        },
      },
    ],
  });

  const selectTicket = (id) => {
    if (selectedTicket === id) {
      setSelectedTicket(undefined);
    } else {
      setSelectedTicket(id);
    }
  };

  const selectShuttle = (id) => {
    if (selectedShuttle === id) {
      setSelectedShuttle(undefined);
    } else {
      setSelectedShuttle(id);
    }
  };

  const ressaisir = () => {
    localStorage.removeItem("tickets");
    setRefresh(!refresh);
  };

  const loadData = async () => {
    try {
      const res = await ajaxGet("");
      const tmp = {
        id: res.data.id,
        name: res.data.name,
        description: res.data.description,
      };
      setEvent(tmp);
      const tmpShotguns = res.data.shotguns;
      setShotguns(tmpShotguns);
      const tmpShuttles = { ...shuttles };
      for (const shotgun in shotguns) {
        let res = await ajaxGet(`/shotguns/${shotgun.id}/shuttles`);
        tmpShuttles[shotgun.id] = res.data;
      }
      setShuttles(tmpShuttles);
    } catch (e) {}
  };

  useEffect(() => {
    const tickets = localStorage.getItem("tickets");
    if (tickets) {
      const ticketsArray = JSON.parse(tickets);
      setTickets(ticketsArray);
    } else {
      navigate("/");
    }
    loadData();
  }, [refresh]);
  return (
    <div
      style={{
        width: "100vw",
        backgroundColor: "rgba(255,255,255,0.95)",
        padding: "5rem",
        display: "flex",
        flexDirection: "column",
        gap: "3rem",
        boxSizing: "border-box",
      }}
    >
      <div style={{ display: "flex", flexDirection: "column" }}>
        <span style={{ fontSize: "4rem", fontWeight: "bold" }}>
          {event.name}
        </span>
        <span style={{ fontSize: "2rem", fontWeight: "bold" }}>
          {event.description}
        </span>
      </div>
      <div style={{ display: "flex", flexDirection: "column", gap: "1rem" }}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            gap: "1.5rem",
            alignItems: "center",
          }}
        >
          <span
            style={{
              fontSize: "3rem",
              fontWeight: "bold",
              textDecoration: "underline",
            }}
          >
            Billets:
          </span>
          <Button variant="outlined" onClick={() => ressaisir()}>
            Ressaisir billets
          </Button>
        </div>
        <div
          style={{
            overflow: "scroll",
            display: "flex",
            flexDirection: "row",
            gap: "2rem",
            margin: "2rem",
          }}
        >
          {tickets.map((ticket) => {
            if (selectedTicket === ticket.id) {
              return (
                <Box
                  style={{ display: "flex", flexDirection: "column" }}
                  sx={{
                    padding: "0.5rem",
                    borderRadius: 1,
                    bgcolor: "green",
                    "&:hover": {
                      bgcolor: "darkgreen",
                      cursor: "pointer",
                    },
                  }}
                  onClick={() => selectTicket(ticket.id)}
                >
                  <span style={{ fontSize: "1.5rem", fontWeight: "bold" }}>
                    {ticket.email}
                  </span>
                  <span style={{ fontSize: "1rem", fontWeight: "normal" }}>
                    {ticket.product_name}
                  </span>
                  <span>{ticket.code}</span>
                </Box>
              );
            } else {
              return (
                <Box
                  style={{ display: "flex", flexDirection: "column" }}
                  sx={{
                    padding: "0.5rem",
                    borderRadius: 1,
                    bgcolor: "primary.main",
                    "&:hover": {
                      bgcolor: "primary.dark",
                      cursor: "pointer",
                    },
                  }}
                  onClick={() => selectTicket(ticket.id)}
                >
                  <span style={{ fontSize: "1.5rem", fontWeight: "bold" }}>
                    {ticket.email}
                  </span>
                  <span style={{ fontSize: "1rem", fontWeight: "normal" }}>
                    {ticket.product_name}
                  </span>
                  <span>{ticket.code}</span>
                </Box>
              );
            }
          })}
        </div>
      </div>
      <div style={{ display: "flex", flexDirection: "column", gap: "1rem" }}>
        <span
          style={{
            fontSize: "3rem",
            fontWeight: "bold",
            textDecoration: "underline",
          }}
        >
          Shotguns:
        </span>
        {shotguns.map((shotgun) => {
          return (
            <div
              style={{
                margin: "2rem",
                gap: "1rem",
                display: "flex",
                flexDirection: "column",
              }}
            >
              <div
                style={{
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <span style={{ fontSize: "2rem", fontWeight: "bold" }}>
                  {shotgun.name}
                </span>
                <span>
                  Ouvert de
                  {" " +
                    new Intl.DateTimeFormat("fr-FR", {
                      hour: "2-digit",
                      minute: "2-digit",
                      second: "2-digit",
                    }).format(new Date(shotgun.start_date)) +
                    " "}
                  à
                  {" " +
                    new Intl.DateTimeFormat("fr-FR", {
                      hour: "2-digit",
                      minute: "2-digit",
                      second: "2-digit",
                    }).format(new Date(shotgun.end_date))}
                </span>
              </div>
              <div
                style={{
                  overflow: "scroll",
                  display: "flex",
                  flexDirection: "row",
                  gap: "2rem",
                }}
              >
                {shuttles[shotgun.id].map((shuttle) => {
                  if (selectedShuttle === shuttle.id) {
                    return (
                      <Box
                        style={{ display: "flex", flexDirection: "column" }}
                        sx={{
                          padding: "0.5rem",
                          borderRadius: 1,
                          bgcolor: "green",
                          "&:hover": {
                            bgcolor: "darkgreen",
                            cursor: "pointer",
                          },
                        }}
                        onClick={() => selectShuttle(shuttle.id)}
                      >
                        <span
                          style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                        >
                          {shuttle.name}
                        </span>
                        <span
                          style={{ fontSize: "1rem", fontWeight: "normal" }}
                        >
                          {new Intl.DateTimeFormat("fr-FR", {
                            day: "2-digit",
                            month: "2-digit",
                            year: "numeric",
                            hour: "2-digit",
                            minute: "2-digit",
                            second: "2-digit",
                          }).format(new Date(shuttle.date))}
                        </span>
                        <span>
                          {shuttle.bookings_count}/{shuttle.limit_place}
                        </span>
                      </Box>
                    );
                  } else {
                    return (
                      <Box
                        style={{ display: "flex", flexDirection: "column" }}
                        sx={{
                          padding: "0.5rem",
                          borderRadius: 1,
                          bgcolor: "primary.main",
                          "&:hover": {
                            bgcolor: "primary.dark",
                            cursor: "pointer",
                          },
                        }}
                        onClick={() => selectShuttle(shuttle.id)}
                      >
                        <span
                          style={{ fontSize: "1.5rem", fontWeight: "bold" }}
                        >
                          {shuttle.name}
                        </span>
                        <span
                          style={{ fontSize: "1rem", fontWeight: "normal" }}
                        >
                          {new Intl.DateTimeFormat("fr-FR", {
                            day: "2-digit",
                            month: "2-digit",
                            year: "numeric",
                            hour: "2-digit",
                            minute: "2-digit",
                            second: "2-digit",
                          }).format(new Date(shuttle.date))}
                        </span>
                        <span>
                          {shuttle.bookings_count}/{shuttle.limit_place}
                        </span>
                      </Box>
                    );
                  }
                })}
              </div>
            </div>
          );
        })}
      </div>
      <div
        style={{
          width: "100%",
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
        }}
      >
        {selectedShuttle !== undefined && selectedTicket !== undefined ? (
          <Button variant="contained" endIcon={<ConfirmationNumberRounded />}>
            Réserver
          </Button>
        ) : (
          <Button
            variant="outlined"
            endIcon={<ConfirmationNumberRounded />}
            disabled
          >
            Réserver
          </Button>
        )}
      </div>
    </div>
  );
};
export default Shotgun;
