import {useEffect, useState} from "react";
import {getStorageTickets, setStorageTickets} from "../utils/ticket-manager";
import AddTicketsForm from "../components/AddTicketsForm";
import {useNavigate} from "react-router-dom";

const Tickets = () => {
    const navigate = useNavigate();
    const [tickets, setTickets] = useState([]);

    //preload tickets from localstorage
    useEffect(() => {
        const tickets = getStorageTickets();
        setTickets(tickets);
    }, []);

    const updateTicketsUi = () => () => {
        const tickets = getStorageTickets();
        setTickets(tickets);
    }
    const deleteTicket = (email, ticket) => () => {
        const tickets = getStorageTickets();
        tickets[email] = tickets[email].filter((t) => t.id !== ticket.id);
        if (tickets[email].length === 0) delete tickets[email];
        setStorageTickets(tickets);
        setTickets(tickets);
    }
    return (
        <div className={"max-w-2xl mx-auto"}>
            <div className={"flex justify-between items-center mb-4"}>
                <p className={"text-xl"}>Mes billets</p>
                <button
                    className={"bg-blue-600 rounded hover:bg-indigo-500 text-white text-sm p-2"}
                    onClick={() => navigate("/")}>Retour
                </button>
            </div>
            {Object.entries(tickets).map(([email, tickets],index) => {
                return (
                    <div key={index} className={""}>
                        <p className={""}>Lié à <span className={"font-bold"}>{email}</span></p>
                        <div className={"flex flex-col gap-2 divide-y divide-gray-200 mt-2"}>
                            {tickets.map((ticket) => {
                                return (
                                    <div key={ticket.code}  className={"flex flex-col gap-2 p-2 border rounded"}>
                                        <div className={"flex flex-row justify-between"}>
                                            <p className={"text-base"}>{ticket.product_name || "Billet pour l'évènement"}</p>
                                            <p className={"text-gray-600"}>#{ticket.code}</p>
                                        </div>
                                        <div className={"flex flex-row justify-end"}>
                                            <button
                                                className={"bg-indigo-600 rounded hover:bg-indigo-500 text-white text-sm p-1"}
                                                onClick={deleteTicket(email, ticket)}>Supprimer
                                            </button>
                                        </div>
                                    </div>
                                );
                            })}
                        </div>
                    </div>
                );
            })}
            {Object.entries(tickets).length === 0
                ? <div>
                    <p className={"text-center text-gray-500 mt-4"}>Aucun billet enregistré</p>
                </div>
                : null}
            <section>
                <p className={'text-xl mt-2'}>Ajouter des billets</p>
                <AddTicketsForm update={updateTicketsUi()}/>

            </section>
        </div>
    );
};

export default Tickets;
