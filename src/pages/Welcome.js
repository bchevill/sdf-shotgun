import {useEffect, useState} from "react";
import {getStorageTicketAsArray} from "../utils/ticket-manager";

const Welcome = () => {
    const [tickets, setTickets] = useState([]);
    useEffect(() => {
        setTickets(getStorageTicketAsArray());
    }, []);
    return (
        <div className={"max-w-2xl mx-auto"}>
            <h1 className={" font-bold text-center"}>Shotgun - SDF 2024</h1>
            <p className={"text-lg font-bold mt-3"}>1. Enregistre tes billets</p>
            <section className={"border border-gray-500 text-gray-700 p-4 rounded grid gap-2"}>
                <p>Pour enregistrer tes billets, il faut :</p>
                <ul className={"ms-4"}>
                    <li>1. Ecrire le code de ton billet <b>(sous le QR code)</b></li>
                    <li>2. Cliquer sur "Ajouter"</li>
                </ul>
                <a href="/tickets"
                   className={"bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded mt-2 text-center"}>Commencer</a>
                <p className={"text-center text-xs"}>Tu as {tickets.length} billets enregistrés</p>
                <p className={"font-bold mt-3"}>Tu as acheté une place exté ?</p>
                <div className={"ms-4"}>
                    Pas besoin de shotgun pour eux, une place est directement réservée dans la même navette que toi ! Toutes les informations t'ont été communiqués par mail.
                </div>
                <span>Une question ? <a href="mailto:soireedesfinaux@assos.utc.fr">soireedesfinaux@assos.utc.fr</a></span>                
            </section>
            <p className={"text-lg font-bold mt-2"}>2. Shotgun</p>
            <section className={"border border-gray-500 text-gray-700 p-4 rounded grid gap-2"}>
                <p>Pour shotgun, tu dois :</p>
                <ul className={"ms-4"}>
                    <li>1. Selectionner le billet que tu veux utiliser</li>
                    <li>2. Selectionner la navette que tu veux prendre</li>
                    <li>3. Cliquer sur "Réserver"</li>
                </ul>
                <a href="/shotguns/9"
                   className={"bg-indigo-500 hover:bg-indigo-700 text-white font-bold py-2 px-4 rounded mt-2 text-center"}>Shotgun !</a>
            </section>
        </div>
    )
}

export default Welcome;