import {useState} from "react";
import {Close} from "@mui/icons-material";
import {ajaxPost} from "../utils/ajax";
import {mergeStorageTickets} from "../utils/ticket-manager";
import {Alert, Snackbar} from "@mui/material";

const AddTicketsForm = (props) => {
    const [codes, setCodes] = useState([]);
    const [currentCode, setCurrentCode] = useState('');
    const [open, setOpen] = useState(undefined);
    const [message, setMessage] = useState("Vos billets ont été importés avec succès !");
    const update = props.update;
    const handleSubmit = async (e) => {
        e.preventDefault();
        let _codes = [...codes]
        if (currentCode.length > 3 && codes.length === 0) {
            setCodes([{code: currentCode.trim()}]);
            _codes.push({code: currentCode.trim()});
            setCurrentCode('');
        }
        const data = {codes: _codes.map((code) => code.code)};
        let tickets = [];
        try {
            let res = await ajaxPost("/tickets/validate", data);
            const errors = [];
            for (const [key, value] of Object.entries(res.data)) {
                if (!("error" in res.data[key])) {
                    tickets.push(res.data[key]);
                } else {
                    errors.push(key);
                }
            }
            setMessage("Vos billets ont été importés avec succès !");
            setOpen("success");
            setCodes(errors.map((code) => ({code, error: "Code pas trouvé"})));
            setCurrentCode('');

            const groupedEmails = {};
            tickets.forEach((ticket) => {
                if (!(ticket.email in groupedEmails))
                    groupedEmails[ticket.email] = [];
                groupedEmails[ticket.email].push(ticket);
            });
            Object.keys(groupedEmails).forEach((key) => {
                mergeStorageTickets(key, groupedEmails[key]);
            });
            update();
        } catch (e) {
            if (e.response.status === 404) {
                setMessage("Aucun billet trouvé avec le code.");
                setOpen("error");
                return;
            }
            setMessage("Une erreur est survenue lors de l'importation des billets.");
            setOpen("error");
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <div>
                <label htmlFor="codes">Code :</label>
                {/*     codes is array of #code   X*/}
                {codes.map((code, index) => (
                    <div key={index}>
                        <div className={"flex justify-between p-2"}>
                            <div className={"flex flex-col"}>
                                <p>{code.code}</p>
                                {code.error && <p className={"text-red-500 text-sm"}>{code.error}</p>}
                            </div>
                            <button onClick={() => {
                                setCodes(codes.filter((_, i) => i !== index))
                            }}><Close></Close></button>
                        </div>
                    </div>
                ))}
                <div className={"flex items-between items-center gap-2"}>
                    <input
                        id="codes"
                        value={currentCode}
                        onChange={(e) => setCurrentCode(e.target.value)}
                        className="w-full border border-gray-300 rounded-md p-2"
                    />
                    <div onClick={() => {
                        if (currentCode.trim().length < 3) return;
                        setCodes([...codes, {code: currentCode.trim()}]);
                        setCurrentCode('');
                    }}>
                    </div>
                </div>
            </div>
            <div className={"flex justify-center mt-4"}>
                <button className={"bg-blue-500 text-white rounded-md p-2"}>Ajouter</button>
            </div>
            <Snackbar
                open={open !== undefined}
                autoHideDuration={2000}
                onClose={() => setOpen(undefined)}
                action={
                    <button onClick={() => setOpen(undefined)}>
                        <Close/>
                    </button>
                }
            ><Alert sx={{width: '100%'}} severity={open}>{message}</Alert>
            </Snackbar>
        </form>
    );
}
export default AddTicketsForm;