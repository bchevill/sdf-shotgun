import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

import '../styles/CustomNavbar.css';

function CustomNavbar() {
  return (
    <div class="CustomNavbar">
      <Navbar expand="lg" className="bg-body-tertiary CustomNavbar" data-bs-theme="dark">
        <Container fluid>
          <Navbar.Brand href="#" class="navbar-brand">Soirée des finaux 2024</Navbar.Brand>
          <Nav>
            <Nav.Link href="https://assos.utc.fr/soireedesfinaux/">Retour au site</Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </div>
  );
}

export default CustomNavbar;