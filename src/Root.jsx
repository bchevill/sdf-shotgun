import {Outlet} from "react-router-dom";
import {getStorageTickets, mergeStorageTickets} from "./utils/ticket-manager";
import {ajaxGet, ajaxPost} from "./utils/ajax";
import {useDispatch} from "react-redux";
import {saveEvent} from "./store/store";
import {useEffect} from "react";
import CustomNavbar from "./components/CustomNavbar";

const Root = () => {
    const dispatch = useDispatch();
    //load event into store event
    const loadEvent = async () => {
        try {
            const res = await ajaxGet("");
            dispatch(saveEvent(res.data));
        } catch (e) {
            console.log(e);
        }
    }
    useEffect(() => {
        loadEvent();
        ensureTicketsOk();
    }, []);

    const ensureTicketsOk = async () => {
            const tickets = getStorageTickets();
            //get all codes
            const codes = [];
            Object.keys(tickets).forEach((key) => {
                tickets[key].forEach((ticket) => {
                    codes.push({code: ticket.code, email: key});
                })
            });
            //split codes by pools of max 5 codes with same email
            const codesPools = [];
            let pool = [];
            let email = "";
            for (let i = 0; i < codes.length; i++) {
                const code = codes[i];
                if (pool.length === 5 || email !== code.email) {
                    if (pool.length !== 0) {
                        codesPools.push(pool);
                        pool = [];
                        email = code.email;
                    }
                }
                pool.push(code);
            }
            if (pool.length > 0) {
                codesPools.push(pool);
            }
            await Promise.all(codesPools.map(async (pool) => {
                try {
                    const res = await ajaxPost("/tickets/validate", {
                        codes: pool.map((code) => code.code),
                        email: pool[0].email
                    });
                    if (res.data) {
                        //res data is {[code]: {error: null|{<ticket object>}}}
                        const validTickets = [];
                        Object.keys(res.data).forEach((key) => {
                            if (res.data[key].error === null) {
                                validTickets.push(res.data[key]);
                            }
                        });
                        //group by email
                        const tickets = {};
                        validTickets.forEach((ticket) => {
                            if (tickets[ticket.email] === undefined) {
                                tickets[ticket.email] = [];
                            }
                            tickets[ticket.email].push(ticket);
                        });
                        //merge with existing tickets (email, ticket)
                        Object.keys(tickets).forEach((key) => {
                            mergeStorageTickets(key, tickets[key]);
                        });
                    }
                } catch (e) {
                    console.log(e);
                }
            }));
    }
    return (
        <div className="h-screen overflow-auto">
            <CustomNavbar/>
            <div className="overflow-auto p-2 main" style={{height: "calc(100vh - 3.5rem)"}}>
                <Outlet/>
            </div>
            <script src="https://cdn.jsdelivr.net/npm/react/umd/react.production.min.js" crossorigin></script>
            <script
                src="https://cdn.jsdelivr.net/npm/react-dom/umd/react-dom.production.min.js"
                crossorigin></script>
            <script
                src="https://cdn.jsdelivr.net/npm/react-bootstrap@next/dist/react-bootstrap.min.js"
                crossorigin></script>
            <link
                rel="stylesheet"
                href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
                integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN"
                crossorigin="anonymous"
            />
        </div>
    )
};

export default Root;