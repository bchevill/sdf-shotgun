FROM node:latest as build-stage
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY ./ .
RUN npm run build
#stage 2
FROM nginx:latest
RUN mkdir /app
#copy from build-stage to nginx with hierarchy and folder structure
COPY --from=build-stage /app/build /app
COPY nginx.conf /etc/nginx/nginx.conf
USER nginx
